package com.sourceallies.hands_on.tdd;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Demo1Test {

    @Mock
    private Addition<String> mockAddr;

    @InjectMocks
    private Demo1 demo1;

    @Before
    public void setup() {
        when(mockAddr.add(any(), any())).thenReturn("WOLDDDD");
    }

    @Test
    public void test1() {
        String s = demo1.concatStrings("1", "2");

        assertThat(s, is("WOLDDDD"));
        verify(mockAddr).add("1", "2");
    }
}
