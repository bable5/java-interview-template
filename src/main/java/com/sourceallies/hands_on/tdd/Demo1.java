package com.sourceallies.hands_on.tdd;

public class Demo1 {
    private final Addition<String> additionFactory;

    public Demo1(Addition additionFactory) {
        this.additionFactory = additionFactory;
    }

    public String concatStrings(String s1, String s2) {
        return additionFactory.add(s1, s2);
    }
}
