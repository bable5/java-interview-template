package com.sourceallies.hands_on.tdd;

@FunctionalInterface
public interface Addition<A> {

    A add(A a1, A a2);
}
